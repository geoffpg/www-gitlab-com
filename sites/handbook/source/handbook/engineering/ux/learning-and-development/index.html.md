---
layout: handbook-page-toc
title: UX Department Learning and Development
description: >-
  This page contains links to internal and external resources that members of the UX Department at GitLab can use to build their skills.
---

#### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Welcome to Learning and Development for the UX Department at GitLab! You are welcome to explore content here for now. We'll update this page when we add this content to GitLab Learn.

The resources in this page are meant to support product designers, researchers, technical writers, and their managers to explore, learn and grow at their own pace. We aim to collect content that spans various skill levels, as well as various levels of depth/commitment. It is recommended that the UX Department engage with the resources here to help them have a successful journey at GitLab and in their career as a whole. 

Most of the resources here are free but any content requiring payment [can be reimbursed following the GitLab reimbursement policies](/handbook/spending-company-money/).

## Recommended resources for all UX Department

### Product design

#### Books
- [The Design of Everyday Things](https://www.amazon.com/Design-Everyday-Things-Revised-Expanded/dp/0465050654) by Don Norman

#### LinkedIn Learning
- [Making User Experience Happen as a Team](https://www.linkedin.com/learning/making-user-experience-happen-as-a-team)

### Design strategy

#### Books
- [Hacking Growth: How Today's Fastest-Growing Companies Drive Breakout Success](https://www.amazon.com/Hacking-Growth-Fastest-Growing-Companies-Breakout/dp/045149721X) by Sean Ellis
- [Better Onboarding](https://abookapart.com/products/better-onboarding) by Krystal Higgins

#### Videos
- [Design Complex Applications: A Framework](https://www.nngroup.com/videos/designing-complex-apps-framework/?utm_source=Alertbox&utm_campaign=1651149249-EMAIL_CAMPAIGN_2020_11_12_08_52_COPY_01&utm_medium=email&utm_term=0_7f29a2b335-1651149249-40578453)

### Design systems

#### Books
- [Building Design Systems: Unify User Experiences through a Shared Design Language](https://www.amazon.com/Building-Design-Systems-Experiences-Language/dp/148424513X) by Sarrah Vesselov, Taurie Davis
- [Expressive Design Systems](https://abookapart.com/products/expressive-design-systems) by Yesenia Perez-Cruz

### UX Research

#### Handbook Links
- [Discovery Resources for Product Managers](/handbook/product/product-manager-role/learning-and-development)

### Jobs to be Done

#### Handbook Links
- [JTBD Resources for Product Managers](/handbook/product/product-manager-role/learning-and-development)

#### Books
- [The Jobs to be Done Playbook](https://www.goodreads.com/book/show/52105688-the-jobs-to-be-done-playbook) by Jim Kalbach


### Growth and Experimentation

#### Books
- [Hacking Growth: How Today's Fastest-Growing Companies Drive Breakout Success](https://www.goodreads.com/book/show/31625067-hacking-growth)
- [Badass: Making Users Awesome](https://www.goodreads.com/book/show/24737268-badass)
- [Better Onboarding](https://www.goodreads.com/book/show/57661776-better-onboarding)


##### Blog posts

- [Small experiments, significant results and learnings](https://about.gitlab.com/blog/2021/04/07/small-experiments-significant-results-and-learnings/)
- [Why iterative software development is critical](https://about.gitlab.com/blog/2021/04/30/why-its-crucial-to-break-things-down-into-smallest-iterations/)

##### Handbook

- [Minimum Viable Experiment](https://about.gitlab.com/handbook/engineering/development/growth/experimentation/#minimum-viable-experiment-mve)
- [How Growth launches experiments](https://about.gitlab.com/handbook/product/growth/#how-growth-launches-experiments)
- [Growth Experiments Knowledge Base](https://about.gitlab.com/direction/growth/#growth-experiments-knowledge-base)

##### Videos

- [GitLab Growth experiment discussion: Adding the "Invite members" option in assignee dropdowns](https://www.youtube.com/watch?v=J5h_SNH3Nt8&list=PL05JrBw4t0Kr_-AowJmbhGk9yj_zIZySf&index=17)
- [Growth Coffee & Learn: Anuj — Author Growth Hacking for Dummies](https://www.youtube.com/watch?v=a3VIrCSq_k8&list=PL05JrBw4t0Kr_-AowJmbhGk9yj_zIZySf&index=4)
- [UX Showcase: New user onboarding strategy](https://www.youtube.com/watch?v=pihqBsophmE&list=PL05JrBw4t0Kq89nFXtkVviaIfYQPptwJz&index=33)
- [UX Showcase: New User Onboarding](https://www.youtube.com/watch?v=NLDzr69D2Pw&list=PL05JrBw4t0Kq89nFXtkVviaIfYQPptwJz&index=56)
- [UX Showcase - Growth - Continuous Onboarding](https://www.youtube.com/watch?v=G345H1Fgpo4&list=PL05JrBw4t0Kq89nFXtkVviaIfYQPptwJz&index=32)
- [UX Showcase: An improved Trial Signup for GitLab.com](https://www.youtube.com/watch?v=5WWZsdJcr2k&list=PL05JrBw4t0Kq89nFXtkVviaIfYQPptwJz&index=149)
- [UX Showcase: Promoting the value of locked features](https://www.youtube.com/watch?v=9_5PFgRFN74&list=PL05JrBw4t0Kq89nFXtkVviaIfYQPptwJz&index=150)
- [UX Showcase: Authoring a Pipeline for the first time](https://youtu.be/wM97njPA8-w)
- [UX Showcase: Simple changes, significant results](https://www.youtube.com/watch?v=uV0alFo_wfI)
- [How to launch product experiments at GitLab](https://youtu.be/rEHxAfxr9eU)
- [UX collaboration on Growth experiments at GitLab](https://youtu.be/wnww8aBoqRY)

#### Other Resources
- [Growth Design](https://lexroman.com/growthdesign)
- [Practicing Growth Design](https://lexroman.com/blog/2019/9/30/practicing-growth-design)
- [What is a Growth Designer?](https://blog.alexaroman.com/what-is-a-growth-designer-1b342d55a358)
- [Growth Designers Resources](https://growthdesigners.co/resources)
- [Growth Design Case Studies](https://growth.design/case-studies/)
- [The Psychology of Design](https://growth.design/psychology/)
- [10 insights into user-centered growth design](https://medium.com/dropbox-design/user-centered-growth-design-97a53d829807)
- [A/B Testing: Concept != Execution](https://booking.design/a-b-testing-concept-execution-b37bf4d744d)
- [Design Like a Scientist](https://youtu.be/XRd6Ddn4ZSY)



### UX writing

- [Microcopy: Discover How Tiny Bits of Text Make Tasty Apps and Websites](https://www.amazon.com/dp/B071S54VLL/ref=cm_sw_em_r_mt_dp_51MKD8EZ2M7KMZYX7N2T) by Niaw de Leon

### Leadership and Communication

#### Books
- [Radical Candor](https://www.amazon.com/Radical-Candor-Kim-Scott/dp/B01KTIEFEE) by Kim Scott
- [Articulating Design Decisions](https://www.goodreads.com/book/show/25520974-articulating-design-decisions) by Tom Greever

#### Handbook Links

- [Leadership at GitLab: Recommended articles](/handbook/leadership/#articles)
- [Leadership at GitLab: Recommended books](/handbook/leadership/#books)

#### LinkedIn Learning
- [Leading with Intelligent Disobediance](https://www.linkedin.com/learning/leading-with-intelligent-disobedience/what-is-intelligent-disobedience?u=2255073)

### Time management and productivity

#### Books
- [Deep Work - Rules for Focused Success in a Distracted World](https://www.amazon.com/dp/B013UWFM52/ref=cm_sw_r_tw_dp_F00CBYJ3VJMPM6A2NYNQ) by Cal Newport

## Other GitLab resources

-[GitLab Book Clubs project](https://gitlab.com/gitlab-com/book-clubs)
- [Product Management Learning and Development](/handbook/product/product-manager-role/learning-and-development/) - a comprehensive list of internal and external resources that Product Managers at GitLab can use to build their skills.
- [Leadership Book clubs](/handbook/leadership/book-clubs/)

## UX Book Club

From time to time, we run an internal book club on a book from one of our resource lists. Join the conversation in `#ux-book-club`.
