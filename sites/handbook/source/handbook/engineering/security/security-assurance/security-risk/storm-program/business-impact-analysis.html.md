---
layout: handbook-page-toc
title: "Business Impact Analysis"
description: "Information about the Business Impact Analsysis process that is carried out periodically by the Security Risk Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

# Business Impact Analysis

The Business Impact Analysis (BIA) is developed as part of the Business Continuity Plan process and is a point-in-time analysis of system components that determines the criticality and potential impact to GitLab's mission-critical processes and data as well as impact to GitLab should the system component become unavailable. This quantitative analysis allows GitLab to establish priority levels for sequencing recovery activities and resources.

## Purpose

The purpose of the BIA is to identify and prioritize system components by correlating them to mission critical processes that support the functioning of GitLab. Using this information to characterize what would be the impact to GitLab, if any of these systems were to be unavailable.

## Scope

The scope of the BIA is the entierty of systems utilized across GitLab. Team members refer to the inventory of systems as the **tech stack**. Note that due to the nature of the information that is maintained in the tech stack, it is not available as a publicly facing document. 

## Roles and Responsibilities

|Role|Responsibility|
|----------|------------------------------|
|[Security Risk Team](/handbook/engineering/security/security-assurance/security-risk/)|Executes an annual BIA. For new systems that have not previously undergone a BIA, a holistic one will be performed. All other systems that have gone through an initial BIA will undergo a targeted BIA process to validate and obtain the most up-to-date data related about it's use at GitLab.|
|[IT Compliance](/handbook/business-technology/it-compliance/)|Utilizes the data obtained from the BIA to drive [Business Continuity Planning](/handbook/business-technology/gitlab-business-continuity-plan/) activities.|

## BIA Procedure

1. A formal BIA questionnaire is distributed to the technical system owners for each system, as listed in the [tech stack](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml). If there are multiple individuals listed, one team member will be selected. The questionnaire will be sent to the team member's GitLab email directly from GitLab's GRC Application, ZenGRC. Additional information on completing this questionnaire can be found on the [ZenGRC Activities](https://about.gitlab.com/handbook/engineering/security/security-assurance/zg-activities.html#completing-zengrc-questionnaires) handbook page. More info on the quetionnaire is available in the video below.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/df5IDS-tGQA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

2. Once all responses have been received, the data will be sanitized and aggregated. Follow-ups with technical owners will be completed as required to ensure the data used is accurate, complete, and objective.

3. Mission critical systems are identified and next steps are taken to ensure that a system recovery/business continuity plan is documented accordingly.

4. On a periodic basis, the BIA is reviewed and will be reperformed. While we do not anticipate significant changes year over year, as part of our due diligence and compliance needs, GitLab is required to make sure that system recovery/business continuity plans are up-to-date so that team members are always prepared to respond to system disruptions or outages.

<div class="panel panel-gitlab-orange">
**Current GitLab Team Members**
{: .panel-heading}
<div class="panel-body">

Additional information about the BIA process can be found in the [BIA Epic](https://gitlab.com/groups/gitlab-com/gl-security/security-assurance/-/epics/36).

</div>
</div>

### BIA Ouputs:

1. Determining data classification and approved operating System usage:   GitLab data and system resources can more clearly be linked to mission critical business processes by way of classifying them based on sensitivity.  These priority levels can be established for sequencing recovery activities and resources. Additionally, the existence of an approved set of operating systems platforms will facilitate ease of management and quick turnaround and repair when they are non-functional.

    - [GitLab’s Data Classification policy covers all aspects of this requirement:](/handbook/engineering/security/data-classification-standard.html)
    - [Approved Operating Systems](/handbook/security/approved_os.html)

1. Determining mission critical business processes and recovery criticality: In this step, GitLab’s mission critical business processes / systems are identified and the impact of a system disruption to those processes is determined along with outage impacts and estimated downtime.  The downtime reflects the maximum, that an organization can tolerate while still maintaining the mission.

    - This is covered in the [priority::1, priority::2, priority::3: Outages and their immediate impact on GitLab customer/user operations](/handbook/business-ops/gitlab-business-continuity-plan#data-continuity-system)

1. Identifying resource requirements:  Realistic recovery efforts require a thorough evaluation of the resources required to resume business processes and related interdependencies as quickly as possible.  Examples of resources that should be identified include software, data files, system components, and vital records.

    - The [Backup and Recovery process](/handbook/engineering/security/security-assurance/security-compliance/guidance/BU.1.01_backup_configuration.html) in GitLab is robust enough to satisfy the above requirement as it relates to GitLab.com.

1. Determining alternate storage and strategies:  Identify any alternate strategies in place to meet expected RTOs.  This includes backup or spare equipment and vendor support contracts.[GitLab alternate storage process, serves to securely store data in an alternate location from source data](/handbook/engineering/security/security-assurance/security-compliance/guidance/BU.1.01_backup_configuration.html)

1. Identifying recovery priorities for system resources based on standards: Adherence to GitLab's agreed upon RTO/ RPO:  Apart from determining the [RTO and RPO](/handbook/business-ops/gitlab-business-continuity-plan#recovery-time-objective-rto-and-recovery-point-objective-rpo), BIA also defines Maximum Tolerable Downtime (MTD)

    - The Maximum Tolerable Downtime (MTD) - represents the total amount of time senior management are willing to accept for a mission/business process outage or disruption and includes all impact considerations.  Determining MTD is important because it could leave continuity planners with imprecise direction on (1) selection of an appropriate recovery method, and (2) the depth of detail which will be required when developing recovery procedures, including their scope and content.

1. Delegating and defining the process:  Designate each incident as critical or non-critical based on the business priority. Compile a list of personnel who must be in place to perform these functions. In times of an occurrence of an incident, a detailed step-by-step approach about how to communicate it to the group, how it is performed, who performs it, and the operational mode of action taken.

The following links show the process carried out at GitLab to cater to this requirement:

- Impact values for assessing category [impact](/handbook/engineering/security/#severity-and-priority-labels-on-security-issues)
- Security issue triage [process](/handbook/engineering/security/#issue-triage)
- Security Incident issues are tagged with the incident label and are further tagged with  [severity::1 severity::2 severity::3 severity::4 labels](https://gitlab.com/gitlab-com/gl-infra/production/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=incident) to determine the severity and accordingly work on resolution]
- Support team contact information - [Quick Reference](/handbook/support/workflows/quick-reference.html)
- On-Call Runbooks - Incident response runbooks for [on-call engineers](/handbook/engineering/infrastructure/incident-management/#on-call-runbooks). .
- Support Team function in the [handbook](/handbook/support/).

## Reporting

The most important part of the Business Impact Analysis is to weigh the exactness of all findings. Communicate the findings to the respective department managers or key personnel to ensure that the assumptions made are in fact accurate and realistic. Once the accuracy of the documented findings has been established and agreed to by all parties, these BIA findings are submitted to GitLab's [e-group](/company/team/structure/#e-group) for approval.

## Plan update and protect from disclosure

The BIA report will be updated based on changes to the organization, information system, or environment of operation and problems encountered during the implementation, execution, or testing.  This plan will be protected from unauthorized disclosure and modification. Finally, all the Business Impact Analysis data will be stored in a safe place for future reference in the event of a disaster.

## Exceptions

There are no systems that are exempt from the BIA procedures. Note that GitLab may procure new systems throughout an annual period. While the Security Risk Team will work towards performing a BIA for new systems in a timely manner, systems may periodically not have a BIA performed until the next annual BIA. 

## References

- [Business Continuity Plan](/handbook/business-technology/gitlab-business-continuity-plan/)
- [Data Classification Standard](/handbook/engineering/security/data-classification-standard.html)
- [GitLab Support Team](/handbook/support/)
- [Incident Response On-call Runbooks](/handbook/engineering/infrastructure/incident-management/#on-call-runbooks)
- [Severity and Priority Labels](/handbook/engineering/security/#severity-and-priority-labels-on-security-issues)
